package com.example.repository;

import java.util.List;

import com.example.model.Student;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAllByOrderByIdAsc();
    List<Student> findAllByOrderByIdDesc();
}
