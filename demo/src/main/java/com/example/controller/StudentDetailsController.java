package com.example.controller;


import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.model.Student;
import com.example.repository.StudentRepository;
import com.example.kafka.KafkaProducer;
// import com.example.kafka.KafkaConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.kafka.annotation.KafkaListener;
import com.fasterxml.jackson.databind.ObjectMapper;



@RestController
@RequestMapping("/students")
public class StudentDetailsController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
	private StudentRepository studentRepository;

	@Autowired
    KafkaProducer kafkaProducer;

	@Value("${spring.kafka.consumer.group-id}")
    String kafkaGroupId;

    @Value("${example.kafka.post.student}")
    String postStudentTopic;
 

	@GetMapping("/")
    public ResponseEntity<?> getAllStudents(@RequestParam(name="order",required=false,defaultValue="asc") String order) {
		List<Student> students;
		
		if(order.equals("asc"))
			students=(List<Student>)studentRepository.findAllByOrderByIdAsc();
		else if(order.equals("desc"))
			students=(List<Student>)studentRepository.findAllByOrderByIdDesc();
		else
			return new ResponseEntity<>("Wrong Query", HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(students, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
    public ResponseEntity<?> getStudentById(@PathVariable long id) {
		Optional<Student> result = studentRepository.findById(id);
    	if (result.isPresent()) {
			Student student=result.get();
			return new ResponseEntity<>(student, HttpStatus.OK);
    	} else {
        	return new ResponseEntity<>("No student with this id", HttpStatus.NOT_FOUND);
    	}
	}
    
    @PostMapping("/")
    public ResponseEntity<?> createStudent(@RequestBody Student student) {
        if(student != null) {
			studentRepository.save(student);
			return new ResponseEntity<>("Student Created", HttpStatus.CREATED);
		} else {
			// return "Request does not contain a body";
			return new ResponseEntity<>("Invalid Data", HttpStatus.BAD_REQUEST);
		}
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Long id,@RequestBody Student student){
		if(student != null) {
        	
			Optional<Student> oldStudent = studentRepository.findById(id);
			if(oldStudent.isPresent()){
				Student existingStudent=oldStudent.get();
				if(student.getName()!=null)
					existingStudent.setName(student.getName());
				if(student.getSection()!=null)
					existingStudent.setSection(student.getSection());
				if(student.getRollNo()!=null)
					existingStudent.setRollNo(student.getRollNo());
				if(student.getEmail()!=null)
					existingStudent.setEmail(student.getEmail());
				if(student.getGender()!=null)
					existingStudent.setGender(student.getGender());
				Student newStudent=studentRepository.save(existingStudent);
				return new ResponseEntity<>("Student Details Updated", HttpStatus.OK);
			}	
			else{
				return new ResponseEntity<>("No Student with this Id", HttpStatus.NOT_FOUND);
			}
			
		}
        return new ResponseEntity<>("Invalid Data", HttpStatus.BAD_REQUEST);
		
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long id){
		if(id > 0) {
        	try {
				studentRepository.deleteById(id);
				return new ResponseEntity<>("Student Data Deleted", HttpStatus.OK);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return new ResponseEntity<>("Something is wrong", HttpStatus.INTERNAL_SERVER_ERROR);
			}
    	}
    	return new ResponseEntity<>("Invalid Id", HttpStatus.BAD_REQUEST);
    }

	// Kafka Post Data

	@PostMapping(value = "/kafkaAdd")
	public ResponseEntity<?> addStudentKafka(@RequestBody Student student){
    	
    	try {
        	kafkaProducer.postStudent(postStudentTopic, kafkaGroupId, student);
    	} catch (Exception e){
        	return new ResponseEntity<>("Something is wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    	return new ResponseEntity<>("Added Student Data to Kafka", HttpStatus.OK);
	}

	// Kafka Listen to data
	@KafkaListener(topics = "4igc0qsg-example.kafka.post.student", groupId = "example")
    public void processPostStudent(String studentJSON){
		logger.info("received content = '{}'", studentJSON);
        try{
            ObjectMapper mapper = new ObjectMapper();
            Student student = mapper.readValue(studentJSON, Student.class);
			studentRepository.save(student);
			logger.info("Success process student '{}' with topic '{}'", student.getName(), "example.kafka.post.student");
		
		} catch (Exception e){
            logger.error("An error occurred! '{}'", e.getMessage());
        }
    }

}
