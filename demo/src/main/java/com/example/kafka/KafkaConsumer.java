// package com.example.kafka;


// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.example.model.Student;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.kafka.annotation.KafkaListener;
// import org.springframework.stereotype.Component;
// import javax.transaction.Transactional;

// import com.example.repository.StudentRepository;

// @Component
// @Transactional
// public class KafkaConsumer {
//     private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

//     @Autowired
//     StudentRepository studentRepository;


//     @KafkaListener(topics = "4igc0qsg-inventories.kafka.post.brand", groupId = "example")
//     public void processPostStudent(String brandJSON){
//         logger.info("received content = '{}'", brandJSON);
//         try{
//             ObjectMapper mapper = new ObjectMapper();
//             Student student = mapper.readValue(brandJSON, Student.class);
//             studentRepository.save(student);
//             logger.info("Student Added from kafka to database");
//         } catch (Exception e){
//             logger.error("An error occurred! '{}'", e.getMessage());
//         }
//     }
// }

