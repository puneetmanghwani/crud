package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "`studentdetails`")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Long id;

    @Column(name = "name")
    public String name;

    @Column(name = "section")
    public String section;

    @Column(name = "rollNo")
    public String rollNo;

    @Column(name = "email")
    public String email;

    @Column(name = "gender")
    public String gender;


    public Student(){}

    public Student(long id,String name,String section,String rollNo,String email,String gender){
        this.id=id;
        this.name=name;
        this.section=section;
        this.rollNo=rollNo;
        this.email=email;
        this.gender=gender;
    }

    public String getName() {
        return name;
    }
    public String getSection() {
        return section;
    }
    public String getRollNo() {
        return rollNo;
    }
    public String getEmail() {
        return email;
    }
    public String getGender() {
        return gender;
    }


    public void setName(String name) {
        this.name = name;
    }
    public void setSection(String section) {
        this.section = section;
    }
    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }


    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        builder.append(String.valueOf(id));
        builder.append(", ");
        builder.append(name);
        builder.append(", ");
        builder.append(section);
        builder.append(", ");
        builder.append(rollNo);
        builder.append(", ");
        builder.append(email);
        builder.append(", ");
        builder.append(gender);

        return builder.toString();
    }



}
